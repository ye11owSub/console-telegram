name := "console-telegram"

organization := "ru.peaceart"

libraryDependencies ++= Seq(
  "net.team2xh" %% "onions" % "1.0.1"
)

mainClass in(Compile, run) := Some("examples.ExampleUI")